﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTime : MonoBehaviour
{
    public float spawnTimer = 0;    // Editable var set spawn timer value.
    public float despawnTimer = 0;  // Editable var set despawn timer value.
    public float timeAdded = 0;     // Editable var set time added value.
    public bool pickedUp = false;   // Bool to determine if the object has been picked up. 

    bool spawn = false;             // Var that handles when the object spawns.
    float timeToSpawn;              // Var to reference and hold spawn timer's value.
    float timeToDespawn;            // Var to reference and hold despawn timer's value.

    MeshRenderer mesh;              // Var to reference mesh.
    CapsuleCollider collide;        // Var to reference collider.

    // Start is called before the first frame update
    void Start()
    {
        // Sets mesh and collide vars to the game objects the modify on start.
        mesh = GetComponent<MeshRenderer>();
        collide = GetComponent<CapsuleCollider>();
        
        // Sets spawn and despawn timer's values on start
        timeToSpawn = spawnTimer;
        timeToDespawn = despawnTimer;
        
        // Disables mesh on start.
        mesh.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Disables mesh and collision when the picked up var is true.
        if (pickedUp)
        {
            mesh.enabled = false;
            collide.enabled = false;
        }
        
        // Starts the timer that handles the objects despawn when the spawn var is true.
        if (spawn)
        {
            timeToDespawn -= Time.deltaTime;
        }

        // Starts the timer that handles the objects spawn when the spawn var is false
        // and disables mesh and collision.
        if (!spawn)
        {
            timeToSpawn -= Time.deltaTime;
            mesh.enabled = false;
            collide.enabled = false;
        }
        
        

        // Checks if spawn timer has ended, if so it calls the spawn choice function, sets spawn var to true, 
        // resets the spawn timer's value, enables mesh and collision, and sets the picked up var to false.
        if (timeToSpawn <= 0)
        {
            SpawnChoice();
            spawn = true;
            timeToSpawn = spawnTimer;
            mesh.enabled = true;
            collide.enabled = true;
            pickedUp = false;
        }

        // Checks if despawn timer has ended, if so it sets spawn var to false and resets the despawn timer's value.
        if (timeToDespawn <= 0)
        {
            spawn = false;
            timeToDespawn = despawnTimer;
        }

        // Sets rotation of add time object.
        transform.Rotate(new Vector3(30, 45, 60) * Time.deltaTime);
    }

    // Function that randomly chooses the spawn location of the add time object.
    void SpawnChoice()
    {
        int choice = Random.Range(1, 5);

        if (choice == 1)
        {
            transform.localPosition = new Vector3(Random.Range(-7.0f, 7.0f), 0.75f, Random.Range(-7.0f, 7.0f));
        }

        if (choice == 2)
        {
            transform.localPosition = new Vector3(Random.Range(12.0f, 26f), 1.75f, Random.Range(-7.0f, 7.0f));
        }

        if (choice == 3)
        {
            transform.localPosition = new Vector3(Random.Range(-26f, -12.0f), 1.75f, Random.Range(-7.0f, 7.0f));
        }

        if (choice == 4)
        {
            transform.localPosition = new Vector3(Random.Range(-7.0f, 7.0f), 1.75f, Random.Range(12.0f, 26f));
        }

        if (choice == 5)
        {
            transform.localPosition = new Vector3(Random.Range(-7.0f, 7.0f), 1.75f, Random.Range(-26f, -12.0f));
        }
    }

}