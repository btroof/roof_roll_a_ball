﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpMovement : MonoBehaviour
{
    public double bounceInterval = 0.05;    // Editable var that sets th interval of pick up objects bounce.
    public double bounceHeight = 1;         // Editable var to se bounce height.

    double currentVertical = 0.1;           // Var to determine current vertical position.

    void Update()
    {
        // Moves the object upward in world space 1 unit/1 half second.
        if (currentVertical > 0)
        {
            currentVertical += bounceInterval;
            transform.Translate(Vector3.up * Time.deltaTime / 2, Space.World);

            if (currentVertical > bounceHeight)
            {
                currentVertical = -0.01;
            }
        }

        // Moves the object downward in world space 1 unit/1 half second.
        if (currentVertical < 0)
        {
            currentVertical -= bounceInterval;
            transform.Translate(Vector3.down * Time.deltaTime / 2, Space.World);

            if (currentVertical < -bounceHeight)
            {
                currentVertical = 0.01;
            }
        }

        // Rotates the object.
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}
