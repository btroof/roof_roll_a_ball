﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public double currentVertical = 0.1;
    public bool startGame = false;

    void Update()
    {

        // Move the object upward in world space 1 unit/second.
        if (currentVertical > 0)
        {
            currentVertical += 0.05;
            transform.Translate(Vector3.up * Time.deltaTime/2, Space.World);
            
            if (currentVertical > 1)
            {
                currentVertical = -0.01;
            }
        }
        // Move the object downward in world space 1 unit/second.
        if (currentVertical < 0)
        {
            currentVertical -= 0.05;
            transform.Translate(Vector3.down * Time.deltaTime/2, Space.World);
            
            if (currentVertical < -1)
            {
                currentVertical = 0.01;
            }
        }

        
            


        // Rotate the object.
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}
