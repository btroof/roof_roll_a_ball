﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;   // Var to reference player game object.

    Vector3 offset;             // Vector 3 var to hold offset value.

    void Start()
    {
        // Sets offset valueon start.
        offset = transform.position - player.transform.position;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Makes camera follow player ball.
        transform.position = player.transform.position + offset;
    }
}
