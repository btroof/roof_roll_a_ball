﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    public Text timerText;              // Var to communicate text value to timer.
    public float countdownTime = 60;    // Editable var to set timer count down amount.
    public bool gameOver = false;       // Var to stop timer on game over.

    // Update is called once per frame
    void Update()
    {
        // If the game is not over this statements allows the timer to caountdown and be formatted.
        if (!gameOver)
        {
            if (countdownTime > 0.1)
            {
                // decrements the countdown time var.
                countdownTime -= Time.deltaTime;

                // converts countdown time to minutes
                int minutes = Mathf.FloorToInt(countdownTime / 60F);

                // converts countdown var to seconds.
                int seconds = Mathf.FloorToInt(countdownTime - minutes * 60);

                // combines minutes and seconds for UI display.
                string formattedTime = string.Format("{00:00}:{01:00}", minutes, seconds);

                // assigns formattedTime var to timerText's text property
                timerText.text = formattedTime;
            }
            else
            {
                // ensures timer cannot become negative.
                timerText.text = "00:00";
            }
        }
    }
}
