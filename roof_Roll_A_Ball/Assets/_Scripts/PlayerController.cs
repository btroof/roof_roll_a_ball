﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;         // Editable var to hold speed of player ball.
    public int minimumHeight;   // Editable var to hold minimum valid height of player ball.
    public int winAmountNeeded; // Editable var to hold amount needed to win the game.
    public Text countText;      // Var to communicate text value for count UI.
    public Text winText;        // Var to communicate text value to win UI.
    public Text restartText;    // Var to communicate text value to restart UI.
    public Text loseText;       // var to communicate text value to lose UI.
    public Text timerText;      // Var to communicate text value to timer UI.

    Rigidbody rb;               // Var to communicate with ridgidbody component of player ball.
    int count;                  // Var to hold count of cubes collected by player.

 
    void Start()
    {
        // Sets initial state of game.
        rb = GetComponent<Rigidbody>();
        count = 0;
      
        winText.text = "";
        restartText.text = "";
        loseText.text = "";
        GameObject[] allPickups = GameObject.FindGameObjectsWithTag("PickUp");
        winAmountNeeded = allPickups.Length;
        SetWinGameText();
    }

    void Update()
    {
        // Calls player movement function.
        //MovePlayer();

        rb.AddForce(new Vector3(0, -3, 0) * rb.mass);
        // Calls game over function.
        GameOver();
    }

    /*  When called checks for collision with the pick up game object, 
        sets collided pick up inactive, adds 1 to count, adds to player size, adds mass, 
        and calls SetWinGameText function.
        This function also sets the pickd up var to true in the AddTime script and updates
        the timer's value in the GameTimer script with the value stored in the AddTime script.
    */
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            speed += 1f;
            transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
            SetWinGameText();
            rb.mass += 100;
        }

        if (other.gameObject.CompareTag("AddTime"))
        {
            GameObject.Find("AddTime").GetComponent<AddTime>().pickedUp = true;

            GameObject.Find("TimerText").GetComponent<GameTimer>().countdownTime +=
                GameObject.Find("AddTime").GetComponent<AddTime>().timeAdded;
        }
    }


    // When called sets text of count UI to the 
    // value of count var, sets text of win and 
    // restart UI, calls RidgidbodyConstraints
    // function, and calls the send timer game over function.
    void SetWinGameText()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= winAmountNeeded)
        {
            winText.text = "You Win!";
            ChangeRestartText();
            RidgidbodyConstraints();
            SendTimerGameOver();
        }
    }

    // When called sets text of lose and restart UI, 
    // calls RidgidbodyConstraints function and calls the send timer game over function.
    void SetLoseText()
    {
        loseText.text = "You Lose!";
        ChangeRestartText();
        RidgidbodyConstraints();
        SendTimerGameOver();
    }

    // Freezes position of player ball when called.
    void RidgidbodyConstraints()
    {
        rb.constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
    }

    // Restarts scene when called.
    void RestartScene()
    {
        restartText.text = "";
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Sets the game over var to true in the GameTimer script when called.
    void SendTimerGameOver()
    {
        GameObject.Find("TimerText").GetComponent<GameTimer>().gameOver = true;
    }

    // Checks and sets game over when called.
    void GameOver()
    {
        // Checks ball height, calls SetLoseText function if too low.
        if (GetComponent<Transform>().position.y <= minimumHeight || timerText.text == "00:00")
        {
            SetLoseText();
        }

        // checks if ball is too low or if count of collected cubes has reached max, then checks
        // for spacebar input before calling RestartScene function
        if (GetComponent<Transform>().position.y <= minimumHeight || count >= winAmountNeeded || timerText.text == "00:00")
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                RestartScene();
            }
        }
    }

    // Sets the restart text when called.
    void ChangeRestartText()
    {
        restartText.text = "Press ENTER to Restart!";
    }

    // This function is not used but was kept for future reference.
    /*void MovePlayer()
    {
        // local vars to get input from directional pad.
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        // uses input vars to set direction of player movement.
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        // adds editable amount of force.
        rb.AddForce(movement * speed);
    }*/
}