﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenterOfRotation : MonoBehaviour
{
    public float rotationSpeed = 10; // Editable var to hold rotation speed.

    int upDownRotatation = 0;        // Var to set max/min values for up and down rotation of play area.
    int leftRightRotation = 0;       // Var to set max/min values for left right rotation of play area.

    // Update is called once per frame
    void Update()
    {
        // Calls tilt direction function every frame.
            TiltDirection();
    }

    // Function that determines player input from either WSAD or arrow keys and then rotates play field in the desired direction.
    void TiltDirection()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            if (leftRightRotation < 75)
            {
                leftRightRotation += 1;
                transform.Rotate(new Vector3(0, 0, 1) * rotationSpeed * Time.deltaTime);
            }
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            if (leftRightRotation > -75)
            {
                leftRightRotation -= 1;
                transform.Rotate(new Vector3(0, 0, -1) * rotationSpeed * Time.deltaTime);
            }
        }

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            if (upDownRotatation < 75)
            {
                upDownRotatation += 1;
                transform.Rotate(new Vector3(1, 0, 0) * rotationSpeed * Time.deltaTime);
            }
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            if (upDownRotatation > -75)
            {
                upDownRotatation -= 1;
                transform.Rotate(new Vector3(-1, 0, 0) * rotationSpeed * Time.deltaTime);
            }
        }
    }
}
